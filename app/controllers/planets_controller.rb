class PlanetsController < ApplicationController

  def create
    planet = Planet.new(planet_params)
    if planet.save
      render json: planet, status: 201
    else
      render json: { errors: planet.errors }, status: 422
    end
  end

  def index
    planet = if planet_params[:name].present?
      Planet.where(name: planet_params[:name]).first
    else
      Planet.all
    end
    render json: planet, status: 200
  end

  def show
    planet = Planet.where(id: planet_params[:id]).first
    if planet.present?
      render json: planet, status: 200
    else
      render json: { }, status: 404
    end
  end

  def update
    planet = Planet.where(id: planet_params[:id]).first
    if planet.update(planet_params)
      render json: planet, status: 200
    else
      render json: { errors: planet.errors }, status: 417
    end
  end

  def destroy
    planet = Planet.where(id: planet_params[:id]).first
    if planet.present?
      planet.destroy
      render json: planet, status: 200
    else
      render json: { errors: 'not found!' }, status: 500
    end
  end

  private

  def planet_params
    params.permit(:id, :name, :climate, :terrain)
  end
end
