require 'net/http'

class Planet
  include Mongoid::Document

  field :name, type: String
  field :climate, type: String
  field :terrain, type: String
  field :count, type: Integer

  validates_presence_of :name, :climate, :terrain

  before_save :set_count

  def set_count
    return if self.count.present?
    url = "https://swapi.co/api/planets/?format=json&search=#{self.name}".gsub(' ', '%20')
    uri = URI(url)
    body = JSON.parse(Net::HTTP.get(uri), symbolize_names: true)
    self.count = body[:results].first[:films].count
  end

end
