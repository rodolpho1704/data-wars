require 'rails_helper'

RSpec.describe 'Planet', type: :request do

  describe 'POST /planets' do
    name = 'Hoth'
    climate = 'frozen'
    terrain = 'tundra, ice caves, mountain ranges'


    before do
      mock_create(name)
      post '/planets', params: { name: name, climate: climate, terrain: terrain }
    end

    context 'when create a planet' do

      it 'returns the planet name' do
        expect(json_body[:name]).to eq('Hoth')
      end
      it 'returns the planet climate' do
        expect(json_body[:climate]).to eq('frozen')
      end
      it 'returns the planet terrain' do
        expect(json_body[:terrain]).to eq('tundra, ice caves, mountain ranges')
      end
      it 'returns the planet count' do
        expect(json_body[:count]).to eq(2)
      end
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the planet is not saved' do
      it 'returns status code 422' do
        post '/planets', params: { climate: climate }
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        post '/planets', params: { climate: climate }
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'GET /planets' do
    context 'when exists one or more planets' do
      let(:planet) { create(:planet, name: 'Alderaan', count: 3) }
      let(:planet_id) { planet.id }

      before do
        planet.save
        get '/planets'
      end

      it 'returns one planet' do
        expect(json_body.first[:_id][:$oid]).to eq(planet.id.to_s)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when planet not exists' do

      before do
        get '/planets'
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns on empty array' do
        expect(response.body).to eq("[]")
      end
    end
  end

  describe 'GET /planets/:name' do
    context 'when the planet exists' do
      let(:planet) { create(:planet, name: 'Alderaan', count: 3) }
      let(:other_planet) { create(:planet, name: 'Hoth', count: 2) }
      let(:planet_id) { planet.id }

      before do
        get "/planets/?name=#{planet.name}"
      end

      it 'returns a only planet with correct name' do
        expect(json_body[:name]).to eq('Alderaan')
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'GET /planets/:id' do
    context 'when the planet exists' do
      let(:planet) { create(:planet, name: 'Alderaan', count: 3) }
      let(:planet_id) { planet.id }

      before do
        planet.save
        get "/planets/#{planet_id.to_s}"
      end

      it 'returns a planet' do
        expect(json_body[:_id][:$oid]).to eq(planet_id.to_s)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the planet not exists' do

      before do
        get "/planets/12312312312"
      end

      it 'returns a empty hash' do
        expect(response.body).to eq("{}")
      end

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'PUT /planets/:id' do
    context 'when update a planet' do
      let(:planet) { create(:planet, name: 'Alderaan', count: 3) }
      let(:planet_id) { planet.id }

      before do
        put "/planets/#{planet_id}", params: { name: 'Hoth' }
      end

      it 'returns the name updated' do
        expect(json_body[:name]).to eq('Hoth')
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when planet update fails' do
      let(:planet) { create(:planet, name: 'Alderaan', count: 3) }
      let(:planet_id) { planet.id }

      before do
        put "/planets/#{planet_id}", params: { name: nil }
      end

      it 'returns status code 417' do

        expect(response).to have_http_status(417)
      end

      it 'returns a json data for the errors' do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /planets/:id' do
    context 'when delete a planet' do
      let(:planet_for_delete) { create(:planet, name: 'Alderaan', count: 3) }
      let(:id_for_delete) { planet_for_delete.id }

      before do
        planet_for_delete.save
        delete "/planets/#{id_for_delete}"
      end

      it 'returns the planet deleted' do
        expect(json_body[:_id][:$oid]).to eq(id_for_delete.to_s)
      end

      it 'returns status 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the planet is not deletd' do

      before do
        delete '/planets/213123123'
      end

      it  'returns status code 500' do
        expect(response).to have_http_status(500)
      end

      it  'returns the json data for the errors' do
        expect(json_body).to have_key(:errors)
      end
    end
  end
end
