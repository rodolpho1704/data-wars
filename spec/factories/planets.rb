FactoryBot.define do
  factory :planet do

    name { 'Alderaan' }
    climate { 'temperate' }
    terrain { 'grasslands, mountains' }
  end
end
