require "rails_helper"

RSpec.describe Planet, type: :model do
  it { is_expected.to have_field(:name).of_type(String) }
  it { is_expected.to have_field(:climate).of_type(String) }
  it { is_expected.to have_field(:terrain).of_type(String) }
  it { is_expected.to have_field(:count).of_type(Integer) }

  describe '#set_count' do
    context 'when create a planet' do
      it 'returns the amount of movies that appeared.' do
        planet = Planet.new(name: 'Alderaan')
        body = {results: [{films: ["foo", "foo"]}]}
        stub_request(:get, "https://swapi.co/api/planets/?format=json&search=#{planet.name}").
          to_return(status: 200, body: body.to_json, headers: {})
        planet.set_count
        expect(planet.count).to eq(body[:results].first[:films].count)
      end
    end
  end
end
