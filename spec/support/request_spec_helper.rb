module RequestSpecHelper
	def json_body
		@json_body ||= JSON.parse(response.body, symbolize_names: true)
	end

	def mock_create(name)
		body = {results: [{films: ["foo", "foo"]}]}
		stub_request(:get, "https://swapi.co/api/planets/?format=json&search=#{name}").
      to_return(status: 200, body: body.to_json, headers: {})
	end
end
